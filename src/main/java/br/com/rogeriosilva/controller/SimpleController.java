package br.com.rogeriosilva.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;

@Controller
public class SimpleController {
	
	@Inject
	private Result result;
	
	
	public void index(){
		
	}
	
	@Get("/simple/hello")
	public void helloworld(){
		result.use(Results.json())
			.from("Hello World...VRaptor 4")
			.serialize();
	}
	
}
